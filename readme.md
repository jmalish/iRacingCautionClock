# iRacing Caution Clock
This is the public repo of the iRacing Caution clock. It is meant to allow an easy place to download the program. The source code is in a private repo.

The program itself is meant to emulate the NASCAR Camping World Truck Series' "new" rules regarding cautions.

### [Download](https://gitlab.com/jmalish/iRacingCautionClock/-/tags/)
Choose latest tag and click the "Download here" to be taken to latest installer

#### [Program help page](https://jmalish.github.io/iRacingCautionClock/)

### References
To connect to iRacing's SDK, I am using [Nick Thissens C# wrapper](https://github.com/NickThissen/iRacingSdkWrapper).  
I am also using yamldotnet to parse iRacing's SDK, which can be found in the nuget package manager.
