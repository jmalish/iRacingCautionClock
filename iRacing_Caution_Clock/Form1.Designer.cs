﻿namespace iRacing_Caution_Clock
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.lblConnectedToiRacing = new System.Windows.Forms.Label();
            this.lblCurrentFlag = new System.Windows.Forms.Label();
            this.lblCurrentLap = new System.Windows.Forms.Label();
            this.lblCautionClockStatus = new System.Windows.Forms.Label();
            this.lblCautionClockExpires = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPgQuickInfo = new System.Windows.Forms.TabPage();
            this.lblVersion = new System.Windows.Forms.Label();
            this.btnTestHotkeys = new System.Windows.Forms.Button();
            this.btnManualCaution = new System.Windows.Forms.Button();
            this.chkControlsCautions = new System.Windows.Forms.CheckBox();
            this.lblUserIsAdmin = new System.Windows.Forms.Label();
            this.tabPgLargeCountdown = new System.Windows.Forms.TabPage();
            this.chkMinimal = new System.Windows.Forms.CheckBox();
            this.btnChangeBgColor = new System.Windows.Forms.Button();
            this.lblCountdownTitle = new System.Windows.Forms.Label();
            this.btnChangeLblColor = new System.Windows.Forms.Button();
            this.lblLargeCounter = new System.Windows.Forms.Label();
            this.tabPgOptions = new System.Windows.Forms.TabPage();
            this.chkStayOnTop = new System.Windows.Forms.CheckBox();
            this.lblDivider2 = new System.Windows.Forms.Label();
            this.lblDivider1 = new System.Windows.Forms.Label();
            this.chkEnableMinWarnings = new System.Windows.Forms.CheckBox();
            this.num1MinHotkey = new System.Windows.Forms.NumericUpDown();
            this.lbl1MinHotkey = new System.Windows.Forms.Label();
            this.num5MinHotkey = new System.Windows.Forms.NumericUpDown();
            this.lbl5MinHotkey = new System.Windows.Forms.Label();
            this.num10MinHotkey = new System.Windows.Forms.NumericUpDown();
            this.lbl10MinHotkey = new System.Windows.Forms.Label();
            this.lblPlayAudioInfo = new System.Windows.Forms.Label();
            this.chkPlayAudioOnCaution = new System.Windows.Forms.CheckBox();
            this.btnTestAudio = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.lblCautionShortcutKey = new System.Windows.Forms.Label();
            this.tabPgTime = new System.Windows.Forms.TabPage();
            this.chkEnableTimer = new System.Windows.Forms.CheckBox();
            this.lblCautionClockCutoffUpdate = new System.Windows.Forms.Label();
            this.numUDLapCutoff = new System.Windows.Forms.NumericUpDown();
            this.lblCurrentTimerLength = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblCautionClockLength = new System.Windows.Forms.Label();
            this.numUDTimeBetween = new System.Windows.Forms.NumericUpDown();
            this.tabPgLaps = new System.Windows.Forms.TabPage();
            this.chkCautionTriggered5 = new System.Windows.Forms.CheckBox();
            this.chkCautionTriggered4 = new System.Windows.Forms.CheckBox();
            this.chkCautionTriggered3 = new System.Windows.Forms.CheckBox();
            this.chkCautionTriggered2 = new System.Windows.Forms.CheckBox();
            this.chkCautionTriggered1 = new System.Windows.Forms.CheckBox();
            this.chkCautionLap5 = new System.Windows.Forms.CheckBox();
            this.numUDCautionLap5 = new System.Windows.Forms.NumericUpDown();
            this.chkCautionLap4 = new System.Windows.Forms.CheckBox();
            this.numUDCautionLap4 = new System.Windows.Forms.NumericUpDown();
            this.chkCautionLap3 = new System.Windows.Forms.CheckBox();
            this.numUDCautionLap3 = new System.Windows.Forms.NumericUpDown();
            this.chkCautionLap2 = new System.Windows.Forms.CheckBox();
            this.numUDCautionLap2 = new System.Windows.Forms.NumericUpDown();
            this.chkCautionLap1 = new System.Windows.Forms.CheckBox();
            this.numUDCautionLap1 = new System.Windows.Forms.NumericUpDown();
            this.lblLapsEnableToAdd = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPgQuickInfo.SuspendLayout();
            this.tabPgLargeCountdown.SuspendLayout();
            this.tabPgOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num1MinHotkey)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num5MinHotkey)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.num10MinHotkey)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tabPgTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUDLapCutoff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUDTimeBetween)).BeginInit();
            this.tabPgLaps.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUDCautionLap5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUDCautionLap4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUDCautionLap3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUDCautionLap2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUDCautionLap1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblConnectedToiRacing
            // 
            this.lblConnectedToiRacing.AutoSize = true;
            this.lblConnectedToiRacing.Location = new System.Drawing.Point(15, 15);
            this.lblConnectedToiRacing.Name = "lblConnectedToiRacing";
            this.lblConnectedToiRacing.Size = new System.Drawing.Size(121, 13);
            this.lblConnectedToiRacing.TabIndex = 0;
            this.lblConnectedToiRacing.Text = "Connecting to iRacing...";
            // 
            // lblCurrentFlag
            // 
            this.lblCurrentFlag.AutoSize = true;
            this.lblCurrentFlag.Location = new System.Drawing.Point(15, 69);
            this.lblCurrentFlag.Name = "lblCurrentFlag";
            this.lblCurrentFlag.Size = new System.Drawing.Size(73, 13);
            this.lblCurrentFlag.TabIndex = 0;
            this.lblCurrentFlag.Text = "Current Flag: -";
            // 
            // lblCurrentLap
            // 
            this.lblCurrentLap.AutoSize = true;
            this.lblCurrentLap.Location = new System.Drawing.Point(15, 96);
            this.lblCurrentLap.Name = "lblCurrentLap";
            this.lblCurrentLap.Size = new System.Drawing.Size(49, 13);
            this.lblCurrentLap.TabIndex = 0;
            this.lblCurrentLap.Text = "Lap - of -";
            // 
            // lblCautionClockStatus
            // 
            this.lblCautionClockStatus.AutoSize = true;
            this.lblCautionClockStatus.Location = new System.Drawing.Point(15, 150);
            this.lblCautionClockStatus.Name = "lblCautionClockStatus";
            this.lblCautionClockStatus.Size = new System.Drawing.Size(129, 13);
            this.lblCautionClockStatus.TabIndex = 0;
            this.lblCautionClockStatus.Text = "Caution Clock: Not Active";
            // 
            // lblCautionClockExpires
            // 
            this.lblCautionClockExpires.AutoSize = true;
            this.lblCautionClockExpires.Location = new System.Drawing.Point(15, 123);
            this.lblCautionClockExpires.Name = "lblCautionClockExpires";
            this.lblCautionClockExpires.Size = new System.Drawing.Size(90, 13);
            this.lblCautionClockExpires.TabIndex = 0;
            this.lblCautionClockExpires.Text = "Clock expires in: -";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPgQuickInfo);
            this.tabControl1.Controls.Add(this.tabPgLargeCountdown);
            this.tabControl1.Controls.Add(this.tabPgOptions);
            this.tabControl1.Controls.Add(this.tabPgTime);
            this.tabControl1.Controls.Add(this.tabPgLaps);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(409, 261);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPgQuickInfo
            // 
            this.tabPgQuickInfo.Controls.Add(this.lblVersion);
            this.tabPgQuickInfo.Controls.Add(this.btnTestHotkeys);
            this.tabPgQuickInfo.Controls.Add(this.btnManualCaution);
            this.tabPgQuickInfo.Controls.Add(this.chkControlsCautions);
            this.tabPgQuickInfo.Controls.Add(this.lblConnectedToiRacing);
            this.tabPgQuickInfo.Controls.Add(this.lblCautionClockExpires);
            this.tabPgQuickInfo.Controls.Add(this.lblCautionClockStatus);
            this.tabPgQuickInfo.Controls.Add(this.lblUserIsAdmin);
            this.tabPgQuickInfo.Controls.Add(this.lblCurrentFlag);
            this.tabPgQuickInfo.Controls.Add(this.lblCurrentLap);
            this.tabPgQuickInfo.Location = new System.Drawing.Point(4, 22);
            this.tabPgQuickInfo.Name = "tabPgQuickInfo";
            this.tabPgQuickInfo.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgQuickInfo.Size = new System.Drawing.Size(401, 235);
            this.tabPgQuickInfo.TabIndex = 0;
            this.tabPgQuickInfo.Text = "Quick Info";
            this.tabPgQuickInfo.UseVisualStyleBackColor = true;
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(321, 217);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(72, 13);
            this.lblVersion.TabIndex = 14;
            this.lblVersion.Text = "Version: 1.3.2";
            // 
            // btnTestHotkeys
            // 
            this.btnTestHotkeys.Location = new System.Drawing.Point(286, 46);
            this.btnTestHotkeys.Name = "btnTestHotkeys";
            this.btnTestHotkeys.Size = new System.Drawing.Size(103, 25);
            this.btnTestHotkeys.TabIndex = 2;
            this.btnTestHotkeys.Text = "Test Countdown";
            this.btnTestHotkeys.UseVisualStyleBackColor = true;
            this.btnTestHotkeys.Click += new System.EventHandler(this.btnTestHotkeys_Click);
            // 
            // btnManualCaution
            // 
            this.btnManualCaution.Location = new System.Drawing.Point(286, 15);
            this.btnManualCaution.Name = "btnManualCaution";
            this.btnManualCaution.Size = new System.Drawing.Size(103, 25);
            this.btnManualCaution.TabIndex = 1;
            this.btnManualCaution.Text = "Manual Caution";
            this.btnManualCaution.UseVisualStyleBackColor = true;
            this.btnManualCaution.Click += new System.EventHandler(this.btnManualCaution_Click);
            // 
            // chkControlsCautions
            // 
            this.chkControlsCautions.AutoSize = true;
            this.chkControlsCautions.Location = new System.Drawing.Point(286, 146);
            this.chkControlsCautions.Name = "chkControlsCautions";
            this.chkControlsCautions.Size = new System.Drawing.Size(103, 17);
            this.chkControlsCautions.TabIndex = 3;
            this.chkControlsCautions.Text = "Control Cautions";
            this.chkControlsCautions.UseVisualStyleBackColor = true;
            this.chkControlsCautions.CheckedChanged += new System.EventHandler(this.chkControlsCautions_CheckedChanged);
            // 
            // lblUserIsAdmin
            // 
            this.lblUserIsAdmin.AutoSize = true;
            this.lblUserIsAdmin.Location = new System.Drawing.Point(15, 42);
            this.lblUserIsAdmin.Name = "lblUserIsAdmin";
            this.lblUserIsAdmin.Size = new System.Drawing.Size(45, 13);
            this.lblUserIsAdmin.TabIndex = 0;
            this.lblUserIsAdmin.Text = "Admin: -";
            // 
            // tabPgLargeCountdown
            // 
            this.tabPgLargeCountdown.BackColor = System.Drawing.Color.White;
            this.tabPgLargeCountdown.Controls.Add(this.chkMinimal);
            this.tabPgLargeCountdown.Controls.Add(this.btnChangeBgColor);
            this.tabPgLargeCountdown.Controls.Add(this.lblCountdownTitle);
            this.tabPgLargeCountdown.Controls.Add(this.btnChangeLblColor);
            this.tabPgLargeCountdown.Controls.Add(this.lblLargeCounter);
            this.tabPgLargeCountdown.Location = new System.Drawing.Point(4, 22);
            this.tabPgLargeCountdown.Name = "tabPgLargeCountdown";
            this.tabPgLargeCountdown.Padding = new System.Windows.Forms.Padding(3);
            this.tabPgLargeCountdown.Size = new System.Drawing.Size(401, 235);
            this.tabPgLargeCountdown.TabIndex = 1;
            this.tabPgLargeCountdown.Text = "Large Countdown Timer";
            // 
            // chkMinimal
            // 
            this.chkMinimal.AutoSize = true;
            this.chkMinimal.Location = new System.Drawing.Point(45, 16);
            this.chkMinimal.Name = "chkMinimal";
            this.chkMinimal.Size = new System.Drawing.Size(61, 17);
            this.chkMinimal.TabIndex = 1;
            this.chkMinimal.Text = "Minimal";
            this.chkMinimal.UseVisualStyleBackColor = true;
            this.chkMinimal.CheckedChanged += new System.EventHandler(this.chkMinimal_CheckedChanged);
            // 
            // btnChangeBgColor
            // 
            this.btnChangeBgColor.Location = new System.Drawing.Point(114, 12);
            this.btnChangeBgColor.Name = "btnChangeBgColor";
            this.btnChangeBgColor.Size = new System.Drawing.Size(121, 23);
            this.btnChangeBgColor.TabIndex = 2;
            this.btnChangeBgColor.Text = "Edit Background Color";
            this.btnChangeBgColor.UseVisualStyleBackColor = true;
            this.btnChangeBgColor.Click += new System.EventHandler(this.btnChangeBgColor_Click);
            // 
            // lblCountdownTitle
            // 
            this.lblCountdownTitle.AutoSize = true;
            this.lblCountdownTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblCountdownTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountdownTitle.Location = new System.Drawing.Point(21, 38);
            this.lblCountdownTitle.Name = "lblCountdownTitle";
            this.lblCountdownTitle.Size = new System.Drawing.Size(350, 55);
            this.lblCountdownTitle.TabIndex = 3;
            this.lblCountdownTitle.Text = "Caution Clock:";
            // 
            // btnChangeLblColor
            // 
            this.btnChangeLblColor.Location = new System.Drawing.Point(241, 12);
            this.btnChangeLblColor.Name = "btnChangeLblColor";
            this.btnChangeLblColor.Size = new System.Drawing.Size(100, 23);
            this.btnChangeLblColor.TabIndex = 3;
            this.btnChangeLblColor.Text = "Edit Number Color";
            this.btnChangeLblColor.UseVisualStyleBackColor = true;
            this.btnChangeLblColor.Click += new System.EventHandler(this.btnChangeLblColor_Click);
            // 
            // lblLargeCounter
            // 
            this.lblLargeCounter.AutoSize = true;
            this.lblLargeCounter.BackColor = System.Drawing.Color.Transparent;
            this.lblLargeCounter.Font = new System.Drawing.Font("Arial", 100F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLargeCounter.Location = new System.Drawing.Point(-4, 66);
            this.lblLargeCounter.Name = "lblLargeCounter";
            this.lblLargeCounter.Size = new System.Drawing.Size(409, 155);
            this.lblLargeCounter.TabIndex = 0;
            this.lblLargeCounter.Text = "00:00";
            // 
            // tabPgOptions
            // 
            this.tabPgOptions.Controls.Add(this.chkStayOnTop);
            this.tabPgOptions.Controls.Add(this.lblDivider2);
            this.tabPgOptions.Controls.Add(this.lblDivider1);
            this.tabPgOptions.Controls.Add(this.chkEnableMinWarnings);
            this.tabPgOptions.Controls.Add(this.num1MinHotkey);
            this.tabPgOptions.Controls.Add(this.lbl1MinHotkey);
            this.tabPgOptions.Controls.Add(this.num5MinHotkey);
            this.tabPgOptions.Controls.Add(this.lbl5MinHotkey);
            this.tabPgOptions.Controls.Add(this.num10MinHotkey);
            this.tabPgOptions.Controls.Add(this.lbl10MinHotkey);
            this.tabPgOptions.Controls.Add(this.lblPlayAudioInfo);
            this.tabPgOptions.Controls.Add(this.chkPlayAudioOnCaution);
            this.tabPgOptions.Controls.Add(this.btnTestAudio);
            this.tabPgOptions.Controls.Add(this.numericUpDown1);
            this.tabPgOptions.Controls.Add(this.lblCautionShortcutKey);
            this.tabPgOptions.Location = new System.Drawing.Point(4, 22);
            this.tabPgOptions.Name = "tabPgOptions";
            this.tabPgOptions.Size = new System.Drawing.Size(401, 235);
            this.tabPgOptions.TabIndex = 2;
            this.tabPgOptions.Text = "Options";
            this.tabPgOptions.UseVisualStyleBackColor = true;
            // 
            // chkStayOnTop
            // 
            this.chkStayOnTop.AutoSize = true;
            this.chkStayOnTop.Location = new System.Drawing.Point(9, 12);
            this.chkStayOnTop.Name = "chkStayOnTop";
            this.chkStayOnTop.Size = new System.Drawing.Size(86, 17);
            this.chkStayOnTop.TabIndex = 1;
            this.chkStayOnTop.Text = "Stay On Top";
            this.chkStayOnTop.UseVisualStyleBackColor = true;
            this.chkStayOnTop.CheckedChanged += new System.EventHandler(this.chkStayOnTop_CheckedChanged_1);
            // 
            // lblDivider2
            // 
            this.lblDivider2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDivider2.Location = new System.Drawing.Point(30, 151);
            this.lblDivider2.Name = "lblDivider2";
            this.lblDivider2.Size = new System.Drawing.Size(326, 2);
            this.lblDivider2.TabIndex = 19;
            this.lblDivider2.Text = "label2";
            // 
            // lblDivider1
            // 
            this.lblDivider1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblDivider1.Location = new System.Drawing.Point(30, 65);
            this.lblDivider1.Name = "lblDivider1";
            this.lblDivider1.Size = new System.Drawing.Size(326, 2);
            this.lblDivider1.TabIndex = 19;
            this.lblDivider1.Text = "label2";
            // 
            // chkEnableMinWarnings
            // 
            this.chkEnableMinWarnings.AutoSize = true;
            this.chkEnableMinWarnings.Location = new System.Drawing.Point(200, 94);
            this.chkEnableMinWarnings.Name = "chkEnableMinWarnings";
            this.chkEnableMinWarnings.Size = new System.Drawing.Size(158, 30);
            this.chkEnableMinWarnings.TabIndex = 3;
            this.chkEnableMinWarnings.Text = "Enable Warning Hotkeys\r\nAffects 10, 5, and 1 minutes";
            this.chkEnableMinWarnings.UseVisualStyleBackColor = true;
            this.chkEnableMinWarnings.CheckedChanged += new System.EventHandler(this.chkEnableMinWarnings_CheckedChanged);
            // 
            // num1MinHotkey
            // 
            this.num1MinHotkey.Enabled = false;
            this.num1MinHotkey.Location = new System.Drawing.Point(150, 125);
            this.num1MinHotkey.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.num1MinHotkey.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num1MinHotkey.Name = "num1MinHotkey";
            this.num1MinHotkey.Size = new System.Drawing.Size(34, 20);
            this.num1MinHotkey.TabIndex = 6;
            this.num1MinHotkey.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.num1MinHotkey.ValueChanged += new System.EventHandler(this.num1MinHotkey_ValueChanged);
            // 
            // lbl1MinHotkey
            // 
            this.lbl1MinHotkey.AutoSize = true;
            this.lbl1MinHotkey.Enabled = false;
            this.lbl1MinHotkey.Location = new System.Drawing.Point(6, 127);
            this.lbl1MinHotkey.Name = "lbl1MinHotkey";
            this.lbl1MinHotkey.Size = new System.Drawing.Size(131, 13);
            this.lbl1MinHotkey.TabIndex = 17;
            this.lbl1MinHotkey.Text = "1 Minute Warning Hotkey:";
            // 
            // num5MinHotkey
            // 
            this.num5MinHotkey.Enabled = false;
            this.num5MinHotkey.Location = new System.Drawing.Point(150, 99);
            this.num5MinHotkey.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.num5MinHotkey.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num5MinHotkey.Name = "num5MinHotkey";
            this.num5MinHotkey.Size = new System.Drawing.Size(34, 20);
            this.num5MinHotkey.TabIndex = 5;
            this.num5MinHotkey.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.num5MinHotkey.ValueChanged += new System.EventHandler(this.num5MinHotkey_ValueChanged);
            // 
            // lbl5MinHotkey
            // 
            this.lbl5MinHotkey.AutoSize = true;
            this.lbl5MinHotkey.Enabled = false;
            this.lbl5MinHotkey.Location = new System.Drawing.Point(6, 102);
            this.lbl5MinHotkey.Name = "lbl5MinHotkey";
            this.lbl5MinHotkey.Size = new System.Drawing.Size(131, 13);
            this.lbl5MinHotkey.TabIndex = 17;
            this.lbl5MinHotkey.Text = "5 Minute Warning Hotkey:";
            // 
            // num10MinHotkey
            // 
            this.num10MinHotkey.Enabled = false;
            this.num10MinHotkey.Location = new System.Drawing.Point(150, 73);
            this.num10MinHotkey.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.num10MinHotkey.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.num10MinHotkey.Name = "num10MinHotkey";
            this.num10MinHotkey.Size = new System.Drawing.Size(34, 20);
            this.num10MinHotkey.TabIndex = 4;
            this.num10MinHotkey.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.num10MinHotkey.ValueChanged += new System.EventHandler(this.num10MinHotkey_ValueChanged);
            // 
            // lbl10MinHotkey
            // 
            this.lbl10MinHotkey.AutoSize = true;
            this.lbl10MinHotkey.Enabled = false;
            this.lbl10MinHotkey.Location = new System.Drawing.Point(6, 75);
            this.lbl10MinHotkey.Name = "lbl10MinHotkey";
            this.lbl10MinHotkey.Size = new System.Drawing.Size(137, 13);
            this.lbl10MinHotkey.TabIndex = 15;
            this.lbl10MinHotkey.Text = "10 Minute Warning Hotkey:";
            // 
            // lblPlayAudioInfo
            // 
            this.lblPlayAudioInfo.AutoSize = true;
            this.lblPlayAudioInfo.Location = new System.Drawing.Point(251, 158);
            this.lblPlayAudioInfo.Name = "lblPlayAudioInfo";
            this.lblPlayAudioInfo.Size = new System.Drawing.Size(137, 26);
            this.lblPlayAudioInfo.TabIndex = 12;
            this.lblPlayAudioInfo.Text = "To change volume, use the\r\n volume mixer in Windows";
            // 
            // chkPlayAudioOnCaution
            // 
            this.chkPlayAudioOnCaution.AutoSize = true;
            this.chkPlayAudioOnCaution.Location = new System.Drawing.Point(20, 164);
            this.chkPlayAudioOnCaution.Name = "chkPlayAudioOnCaution";
            this.chkPlayAudioOnCaution.Size = new System.Drawing.Size(149, 17);
            this.chkPlayAudioOnCaution.TabIndex = 7;
            this.chkPlayAudioOnCaution.Text = "Play Audio File on Caution";
            this.chkPlayAudioOnCaution.UseVisualStyleBackColor = true;
            this.chkPlayAudioOnCaution.CheckedChanged += new System.EventHandler(this.chkPlayAudioOnCaution_CheckedChanged);
            // 
            // btnTestAudio
            // 
            this.btnTestAudio.Location = new System.Drawing.Point(175, 160);
            this.btnTestAudio.Name = "btnTestAudio";
            this.btnTestAudio.Size = new System.Drawing.Size(75, 23);
            this.btnTestAudio.TabIndex = 8;
            this.btnTestAudio.Text = "Test Audio";
            this.btnTestAudio.UseVisualStyleBackColor = true;
            this.btnTestAudio.Click += new System.EventHandler(this.btnTestAudio_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(150, 40);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(34, 20);
            this.numericUpDown1.TabIndex = 2;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // lblCautionShortcutKey
            // 
            this.lblCautionShortcutKey.AutoSize = true;
            this.lblCautionShortcutKey.Location = new System.Drawing.Point(6, 42);
            this.lblCautionShortcutKey.Name = "lblCautionShortcutKey";
            this.lblCautionShortcutKey.Size = new System.Drawing.Size(83, 13);
            this.lblCautionShortcutKey.TabIndex = 3;
            this.lblCautionShortcutKey.Text = "Caution Hotkey:";
            // 
            // tabPgTime
            // 
            this.tabPgTime.Controls.Add(this.chkEnableTimer);
            this.tabPgTime.Controls.Add(this.lblCautionClockCutoffUpdate);
            this.tabPgTime.Controls.Add(this.numUDLapCutoff);
            this.tabPgTime.Controls.Add(this.lblCurrentTimerLength);
            this.tabPgTime.Controls.Add(this.label1);
            this.tabPgTime.Controls.Add(this.lblCautionClockLength);
            this.tabPgTime.Controls.Add(this.numUDTimeBetween);
            this.tabPgTime.Location = new System.Drawing.Point(4, 22);
            this.tabPgTime.Name = "tabPgTime";
            this.tabPgTime.Size = new System.Drawing.Size(401, 235);
            this.tabPgTime.TabIndex = 3;
            this.tabPgTime.Text = "Time";
            this.tabPgTime.UseVisualStyleBackColor = true;
            // 
            // chkEnableTimer
            // 
            this.chkEnableTimer.AutoSize = true;
            this.chkEnableTimer.Location = new System.Drawing.Point(15, 17);
            this.chkEnableTimer.Name = "chkEnableTimer";
            this.chkEnableTimer.Size = new System.Drawing.Size(136, 17);
            this.chkEnableTimer.TabIndex = 1;
            this.chkEnableTimer.Text = "User Timer for Cautions";
            this.chkEnableTimer.UseVisualStyleBackColor = true;
            // 
            // lblCautionClockCutoffUpdate
            // 
            this.lblCautionClockCutoffUpdate.AutoSize = true;
            this.lblCautionClockCutoffUpdate.Location = new System.Drawing.Point(12, 125);
            this.lblCautionClockCutoffUpdate.Name = "lblCautionClockCutoffUpdate";
            this.lblCautionClockCutoffUpdate.Size = new System.Drawing.Size(16, 13);
            this.lblCautionClockCutoffUpdate.TabIndex = 14;
            this.lblCautionClockCutoffUpdate.Text = "...";
            this.lblCautionClockCutoffUpdate.Visible = false;
            // 
            // numUDLapCutoff
            // 
            this.numUDLapCutoff.Location = new System.Drawing.Point(214, 87);
            this.numUDLapCutoff.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numUDLapCutoff.Name = "numUDLapCutoff";
            this.numUDLapCutoff.Size = new System.Drawing.Size(51, 20);
            this.numUDLapCutoff.TabIndex = 3;
            this.numUDLapCutoff.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // lblCurrentTimerLength
            // 
            this.lblCurrentTimerLength.AutoSize = true;
            this.lblCurrentTimerLength.Location = new System.Drawing.Point(107, 71);
            this.lblCurrentTimerLength.Name = "lblCurrentTimerLength";
            this.lblCurrentTimerLength.Size = new System.Drawing.Size(181, 13);
            this.lblCurrentTimerLength.TabIndex = 13;
            this.lblCurrentTimerLength.Text = "change the counter above to update";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(196, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Caution Clock Lap Cut off (Defualt is 20)";
            // 
            // lblCautionClockLength
            // 
            this.lblCautionClockLength.AutoSize = true;
            this.lblCautionClockLength.Location = new System.Drawing.Point(8, 50);
            this.lblCautionClockLength.Name = "lblCautionClockLength";
            this.lblCautionClockLength.Size = new System.Drawing.Size(237, 13);
            this.lblCautionClockLength.TabIndex = 12;
            this.lblCautionClockLength.Text = "Time Between Cautions in minutes (Default is 20)";
            // 
            // numUDTimeBetween
            // 
            this.numUDTimeBetween.Location = new System.Drawing.Point(249, 48);
            this.numUDTimeBetween.Name = "numUDTimeBetween";
            this.numUDTimeBetween.Size = new System.Drawing.Size(47, 20);
            this.numUDTimeBetween.TabIndex = 2;
            this.numUDTimeBetween.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // tabPgLaps
            // 
            this.tabPgLaps.Controls.Add(this.chkCautionTriggered5);
            this.tabPgLaps.Controls.Add(this.chkCautionTriggered4);
            this.tabPgLaps.Controls.Add(this.chkCautionTriggered3);
            this.tabPgLaps.Controls.Add(this.chkCautionTriggered2);
            this.tabPgLaps.Controls.Add(this.chkCautionTriggered1);
            this.tabPgLaps.Controls.Add(this.chkCautionLap5);
            this.tabPgLaps.Controls.Add(this.numUDCautionLap5);
            this.tabPgLaps.Controls.Add(this.chkCautionLap4);
            this.tabPgLaps.Controls.Add(this.numUDCautionLap4);
            this.tabPgLaps.Controls.Add(this.chkCautionLap3);
            this.tabPgLaps.Controls.Add(this.numUDCautionLap3);
            this.tabPgLaps.Controls.Add(this.chkCautionLap2);
            this.tabPgLaps.Controls.Add(this.numUDCautionLap2);
            this.tabPgLaps.Controls.Add(this.chkCautionLap1);
            this.tabPgLaps.Controls.Add(this.numUDCautionLap1);
            this.tabPgLaps.Controls.Add(this.lblLapsEnableToAdd);
            this.tabPgLaps.Location = new System.Drawing.Point(4, 22);
            this.tabPgLaps.Name = "tabPgLaps";
            this.tabPgLaps.Size = new System.Drawing.Size(401, 235);
            this.tabPgLaps.TabIndex = 4;
            this.tabPgLaps.Text = "Laps";
            this.tabPgLaps.UseVisualStyleBackColor = true;
            // 
            // chkCautionTriggered5
            // 
            this.chkCautionTriggered5.AutoSize = true;
            this.chkCautionTriggered5.Enabled = false;
            this.chkCautionTriggered5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCautionTriggered5.Location = new System.Drawing.Point(207, 138);
            this.chkCautionTriggered5.Name = "chkCautionTriggered5";
            this.chkCautionTriggered5.Size = new System.Drawing.Size(55, 17);
            this.chkCautionTriggered5.TabIndex = 24;
            this.chkCautionTriggered5.Text = "Called";
            this.chkCautionTriggered5.UseVisualStyleBackColor = true;
            // 
            // chkCautionTriggered4
            // 
            this.chkCautionTriggered4.AutoSize = true;
            this.chkCautionTriggered4.Enabled = false;
            this.chkCautionTriggered4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCautionTriggered4.Location = new System.Drawing.Point(207, 115);
            this.chkCautionTriggered4.Name = "chkCautionTriggered4";
            this.chkCautionTriggered4.Size = new System.Drawing.Size(55, 17);
            this.chkCautionTriggered4.TabIndex = 23;
            this.chkCautionTriggered4.Text = "Called";
            this.chkCautionTriggered4.UseVisualStyleBackColor = true;
            // 
            // chkCautionTriggered3
            // 
            this.chkCautionTriggered3.AutoSize = true;
            this.chkCautionTriggered3.Enabled = false;
            this.chkCautionTriggered3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCautionTriggered3.Location = new System.Drawing.Point(207, 92);
            this.chkCautionTriggered3.Name = "chkCautionTriggered3";
            this.chkCautionTriggered3.Size = new System.Drawing.Size(55, 17);
            this.chkCautionTriggered3.TabIndex = 22;
            this.chkCautionTriggered3.Text = "Called";
            this.chkCautionTriggered3.UseVisualStyleBackColor = true;
            // 
            // chkCautionTriggered2
            // 
            this.chkCautionTriggered2.AutoSize = true;
            this.chkCautionTriggered2.Enabled = false;
            this.chkCautionTriggered2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCautionTriggered2.Location = new System.Drawing.Point(207, 69);
            this.chkCautionTriggered2.Name = "chkCautionTriggered2";
            this.chkCautionTriggered2.Size = new System.Drawing.Size(55, 17);
            this.chkCautionTriggered2.TabIndex = 21;
            this.chkCautionTriggered2.Text = "Called";
            this.chkCautionTriggered2.UseVisualStyleBackColor = true;
            // 
            // chkCautionTriggered1
            // 
            this.chkCautionTriggered1.AutoSize = true;
            this.chkCautionTriggered1.Enabled = false;
            this.chkCautionTriggered1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCautionTriggered1.Location = new System.Drawing.Point(207, 46);
            this.chkCautionTriggered1.Name = "chkCautionTriggered1";
            this.chkCautionTriggered1.Size = new System.Drawing.Size(55, 17);
            this.chkCautionTriggered1.TabIndex = 20;
            this.chkCautionTriggered1.Text = "Called";
            this.chkCautionTriggered1.UseVisualStyleBackColor = true;
            // 
            // chkCautionLap5
            // 
            this.chkCautionLap5.AutoSize = true;
            this.chkCautionLap5.Enabled = false;
            this.chkCautionLap5.Location = new System.Drawing.Point(27, 140);
            this.chkCautionLap5.Name = "chkCautionLap5";
            this.chkCautionLap5.Size = new System.Drawing.Size(134, 17);
            this.chkCautionLap5.TabIndex = 9;
            this.chkCautionLap5.Text = "Throw Caution on Lap:";
            this.chkCautionLap5.UseVisualStyleBackColor = true;
            this.chkCautionLap5.CheckedChanged += new System.EventHandler(this.chkCautionLap5_CheckedChanged);
            // 
            // numUDCautionLap5
            // 
            this.numUDCautionLap5.Location = new System.Drawing.Point(161, 139);
            this.numUDCautionLap5.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numUDCautionLap5.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUDCautionLap5.Name = "numUDCautionLap5";
            this.numUDCautionLap5.Size = new System.Drawing.Size(40, 20);
            this.numUDCautionLap5.TabIndex = 10;
            this.numUDCautionLap5.Value = new decimal(new int[] {
            999,
            0,
            0,
            0});
            // 
            // chkCautionLap4
            // 
            this.chkCautionLap4.AutoSize = true;
            this.chkCautionLap4.Enabled = false;
            this.chkCautionLap4.Location = new System.Drawing.Point(27, 115);
            this.chkCautionLap4.Name = "chkCautionLap4";
            this.chkCautionLap4.Size = new System.Drawing.Size(134, 17);
            this.chkCautionLap4.TabIndex = 7;
            this.chkCautionLap4.Text = "Throw Caution on Lap:";
            this.chkCautionLap4.UseVisualStyleBackColor = true;
            this.chkCautionLap4.CheckedChanged += new System.EventHandler(this.chkCautionLap4_CheckedChanged);
            // 
            // numUDCautionLap4
            // 
            this.numUDCautionLap4.Location = new System.Drawing.Point(161, 114);
            this.numUDCautionLap4.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numUDCautionLap4.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUDCautionLap4.Name = "numUDCautionLap4";
            this.numUDCautionLap4.Size = new System.Drawing.Size(40, 20);
            this.numUDCautionLap4.TabIndex = 8;
            this.numUDCautionLap4.Value = new decimal(new int[] {
            999,
            0,
            0,
            0});
            // 
            // chkCautionLap3
            // 
            this.chkCautionLap3.AutoSize = true;
            this.chkCautionLap3.Enabled = false;
            this.chkCautionLap3.Location = new System.Drawing.Point(27, 90);
            this.chkCautionLap3.Name = "chkCautionLap3";
            this.chkCautionLap3.Size = new System.Drawing.Size(134, 17);
            this.chkCautionLap3.TabIndex = 5;
            this.chkCautionLap3.Text = "Throw Caution on Lap:";
            this.chkCautionLap3.UseVisualStyleBackColor = true;
            this.chkCautionLap3.CheckedChanged += new System.EventHandler(this.chkCautionLap3_CheckedChanged);
            // 
            // numUDCautionLap3
            // 
            this.numUDCautionLap3.Location = new System.Drawing.Point(161, 89);
            this.numUDCautionLap3.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numUDCautionLap3.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUDCautionLap3.Name = "numUDCautionLap3";
            this.numUDCautionLap3.Size = new System.Drawing.Size(40, 20);
            this.numUDCautionLap3.TabIndex = 6;
            this.numUDCautionLap3.Value = new decimal(new int[] {
            999,
            0,
            0,
            0});
            // 
            // chkCautionLap2
            // 
            this.chkCautionLap2.AutoSize = true;
            this.chkCautionLap2.Enabled = false;
            this.chkCautionLap2.Location = new System.Drawing.Point(27, 67);
            this.chkCautionLap2.Name = "chkCautionLap2";
            this.chkCautionLap2.Size = new System.Drawing.Size(134, 17);
            this.chkCautionLap2.TabIndex = 3;
            this.chkCautionLap2.Text = "Throw Caution on Lap:";
            this.chkCautionLap2.UseVisualStyleBackColor = true;
            this.chkCautionLap2.CheckedChanged += new System.EventHandler(this.chkCautionLap2_CheckedChanged);
            // 
            // numUDCautionLap2
            // 
            this.numUDCautionLap2.Location = new System.Drawing.Point(161, 66);
            this.numUDCautionLap2.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numUDCautionLap2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUDCautionLap2.Name = "numUDCautionLap2";
            this.numUDCautionLap2.Size = new System.Drawing.Size(40, 20);
            this.numUDCautionLap2.TabIndex = 4;
            this.numUDCautionLap2.Value = new decimal(new int[] {
            999,
            0,
            0,
            0});
            // 
            // chkCautionLap1
            // 
            this.chkCautionLap1.AutoSize = true;
            this.chkCautionLap1.Location = new System.Drawing.Point(27, 42);
            this.chkCautionLap1.Name = "chkCautionLap1";
            this.chkCautionLap1.Size = new System.Drawing.Size(134, 17);
            this.chkCautionLap1.TabIndex = 1;
            this.chkCautionLap1.Text = "Throw Caution on Lap:";
            this.chkCautionLap1.UseVisualStyleBackColor = true;
            this.chkCautionLap1.CheckedChanged += new System.EventHandler(this.chkCautionLap1_CheckedChanged);
            // 
            // numUDCautionLap1
            // 
            this.numUDCautionLap1.Location = new System.Drawing.Point(161, 41);
            this.numUDCautionLap1.Maximum = new decimal(new int[] {
            999,
            0,
            0,
            0});
            this.numUDCautionLap1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numUDCautionLap1.Name = "numUDCautionLap1";
            this.numUDCautionLap1.Size = new System.Drawing.Size(40, 20);
            this.numUDCautionLap1.TabIndex = 2;
            this.numUDCautionLap1.Value = new decimal(new int[] {
            999,
            0,
            0,
            0});
            // 
            // lblLapsEnableToAdd
            // 
            this.lblLapsEnableToAdd.AutoSize = true;
            this.lblLapsEnableToAdd.Location = new System.Drawing.Point(8, 9);
            this.lblLapsEnableToAdd.Name = "lblLapsEnableToAdd";
            this.lblLapsEnableToAdd.Size = new System.Drawing.Size(306, 13);
            this.lblLapsEnableToAdd.TabIndex = 2;
            this.lblLapsEnableToAdd.Text = "Enabling the first caution will allow enabling the next caution lap";
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(409, 261);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(425, 300);
            this.Name = "mainForm";
            this.Text = "iRacing Caution Manager";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.mainForm_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabPgQuickInfo.ResumeLayout(false);
            this.tabPgQuickInfo.PerformLayout();
            this.tabPgLargeCountdown.ResumeLayout(false);
            this.tabPgLargeCountdown.PerformLayout();
            this.tabPgOptions.ResumeLayout(false);
            this.tabPgOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.num1MinHotkey)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num5MinHotkey)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.num10MinHotkey)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tabPgTime.ResumeLayout(false);
            this.tabPgTime.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUDLapCutoff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUDTimeBetween)).EndInit();
            this.tabPgLaps.ResumeLayout(false);
            this.tabPgLaps.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUDCautionLap5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUDCautionLap4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUDCautionLap3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUDCautionLap2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUDCautionLap1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblConnectedToiRacing;
        private System.Windows.Forms.Label lblCurrentFlag;
        private System.Windows.Forms.Label lblCurrentLap;
        private System.Windows.Forms.Label lblCautionClockStatus;
        private System.Windows.Forms.Label lblCautionClockExpires;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPgQuickInfo;
        private System.Windows.Forms.TabPage tabPgLargeCountdown;
        private System.Windows.Forms.Label lblLargeCounter;
        private System.Windows.Forms.Button btnChangeLblColor;
        private System.Windows.Forms.Button btnManualCaution;
        private System.Windows.Forms.Label lblUserIsAdmin;
        private System.Windows.Forms.TabPage tabPgOptions;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label lblCautionShortcutKey;
        private System.Windows.Forms.CheckBox chkControlsCautions;
        private System.Windows.Forms.Button btnTestAudio;
        private System.Windows.Forms.CheckBox chkPlayAudioOnCaution;
        private System.Windows.Forms.Label lblPlayAudioInfo;
        private System.Windows.Forms.Label lblCountdownTitle;
        private System.Windows.Forms.Button btnChangeBgColor;
        private System.Windows.Forms.Label lblDivider2;
        private System.Windows.Forms.Label lblDivider1;
        private System.Windows.Forms.CheckBox chkEnableMinWarnings;
        private System.Windows.Forms.NumericUpDown num1MinHotkey;
        private System.Windows.Forms.Label lbl1MinHotkey;
        private System.Windows.Forms.NumericUpDown num5MinHotkey;
        private System.Windows.Forms.Label lbl5MinHotkey;
        private System.Windows.Forms.NumericUpDown num10MinHotkey;
        private System.Windows.Forms.Label lbl10MinHotkey;
        private System.Windows.Forms.Button btnTestHotkeys;
        private System.Windows.Forms.CheckBox chkMinimal;
        private System.Windows.Forms.TabPage tabPgTime;
        private System.Windows.Forms.Label lblCautionClockCutoffUpdate;
        private System.Windows.Forms.NumericUpDown numUDLapCutoff;
        private System.Windows.Forms.Label lblCurrentTimerLength;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblCautionClockLength;
        private System.Windows.Forms.NumericUpDown numUDTimeBetween;
        private System.Windows.Forms.TabPage tabPgLaps;
        private System.Windows.Forms.NumericUpDown numUDCautionLap1;
        private System.Windows.Forms.Label lblLapsEnableToAdd;
        private System.Windows.Forms.CheckBox chkEnableTimer;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.CheckBox chkCautionLap1;
        private System.Windows.Forms.CheckBox chkCautionLap5;
        private System.Windows.Forms.NumericUpDown numUDCautionLap5;
        private System.Windows.Forms.CheckBox chkCautionLap4;
        private System.Windows.Forms.NumericUpDown numUDCautionLap4;
        private System.Windows.Forms.CheckBox chkCautionLap3;
        private System.Windows.Forms.NumericUpDown numUDCautionLap3;
        private System.Windows.Forms.CheckBox chkCautionLap2;
        private System.Windows.Forms.NumericUpDown numUDCautionLap2;
        private System.Windows.Forms.CheckBox chkCautionTriggered1;
        private System.Windows.Forms.CheckBox chkCautionTriggered5;
        private System.Windows.Forms.CheckBox chkCautionTriggered4;
        private System.Windows.Forms.CheckBox chkCautionTriggered3;
        private System.Windows.Forms.CheckBox chkCautionTriggered2;
        private System.Windows.Forms.CheckBox chkStayOnTop;
    }
}

